@extends('layouts.app')

@section('content')
    <div class="uk-container uk-flex uk-flex-center">
        <div class="uk-card uk-card-body uk-card-default uk-width-1-4">
            <div class="uk-card-title">{{ __('Логин') }}</div>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="uk-margin">
                    <label for="email" class="uk-label">{{ __('E-Mail адресс') }}</label>

                    <input id="email" type="email" class="uk-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="uk-margin">
                    <label for="password" class="uk-label">{{ __('Пароль') }}</label>

                    <input id="password" type="password" class="uk-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="uk-margin">
                    <label>
                        <input class="uk-checkbox" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>{{ __('Запомнить') }}
                    </label>
                </div>

                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-bottom">
                        {{ __('Вход') }}
                    </button>

                    <div class="uk-margin">
                        <a class="uk-link" href="{{ route('password.request') }}">
                            {{ __('Забыли пароль?') }}
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
