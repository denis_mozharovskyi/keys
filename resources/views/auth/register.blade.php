@extends('layouts.app')

@section('content')
<div class="uk-container uk-flex uk-flex-center">
    <div class="uk-card uk-card-body uk-card-default uk-width-1-4">
        <div class="uk-card-title">{{ __('Регистрация') }}</div>

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="uk-margin">
                <label for="name" class="uk-label">{{ __('Имя') }}</label>

                <input id="name" type="text" class="uk-input{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="uk-margin">
                <label for="email" class="uk-label">{{ __('E-Mail адресс') }}</label>

                <input id="email" type="email" class="uk-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="uk-margin">
                <label for="password" class="uk-label">{{ __('Пароль') }}</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="uk-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="uk-margin">
                <label for="password-confirm" class="uk-label">{{ __('Подтвердите пароль') }}</label>

                <div class="uk-margin">
                    <input id="password-confirm" type="password" class="uk-input" name="password_confirmation" required>
                </div>
            </div>

            <div class="uk-margin">
                <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-bottom">
                    {{ __('Зарегестрироваться') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
