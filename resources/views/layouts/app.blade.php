<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@if(Auth::check())
    @include('admin.partials._head')
    <body>
        <div id="app">
            <div class="uk-container">
                <nav class="uk-navbar-container" uk-navbar>
                    <div class="uk-navbar-center">
                        <div class="uk-navbar">
                            <ul class="uk-navbar-nav">
                                <li class="uk-margin-middle-right"><a href="{{ url('/admin') }}"><span class="uk-margin-small-right" uk-icon="home"></span>Главная</a></li>
                                <li>
                                    <a href="{{ url('#') }}"><span class="uk-margin-small-right" uk-icon="plus-circle"></span>Добавить</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav">
                                            <li><a href="{{ url('admin/items/create') }}">Объект</a></li>
                                            <li><a href="{{ url('admin/regions') }}">Район</a></li>
                                            <li><a href="{{ url('admin/employees') }}">Сотрудника</a></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href="{{ url('admin/bids') }}">
                                        @if(!empty($items))
                                            <span><span uk-icon="bell"></span><sub class="uk-margin-small-right">{{ count($items) }}</sub></span>
                                        @else
                                            <span class="uk-margin-small-right" uk-icon="file-edit"></span>
                                        @endif
                                        Заявки
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/recalls') }}"><span class="uk-margin-small-right" uk-icon="comments"></span>Отзывы</a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/jobs') }}"><span class="uk-margin-small-right" uk-icon="file-text"></span>Вакансии</a>
                                </li>
                                <li>
                                    <a href="{{ url('admin/contacts') }}"><span class="uk-margin-small-right" uk-icon="info"></span>Контакты</a>
                                </li>
                            </ul>
                        </div>
                        <div class="uk-navbar">
                            <ul class="uk-navbar-nav">
                                <li>
                                    <a href="{{ url('#') }}"><span class="uk-margin-small-right" uk-icon="user"></span>Admin</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav">
                                            <li><a href="{{ route('logout') }}"><span class="uk-margin-small-right" uk-icon="sing-out"></span>Выход</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="uk-container">
                @yield('content')
            </div>
        </div>
    </body>
@else
    @include('layouts.partials._head')
    <body>
        <div id="app">
            <header>
                <div class="uk-flex uk-flex-center uk-flex-middle">
                    <div class="header__logo uk-margin-small-top"><h1><a href="{{  route('index') }}" class="logo" title='На главную - АН "Ключи"'>На главную - АН "Ключи"</a></h1></div>
                    <div class="div_nav">
                        <nav class="nav">
                            <ul>
                                <li><a href="{{  route('index') }}" class="uk-margin-small-right"><span class="uk-margin-small-right" uk-icon="home"></span>Главная</a></li>
                                <li><a href="{{  route('bids') }}" class="uk-margin-small-right"><span class="uk-margin-small-right" uk-icon="file-edit"></span>Оставить заявку</a></li>
                                <li><a href="{{  route('recalls') }}" class="uk-margin-small-right"><span class="uk-margin-small-right" uk-icon="comments"></span>Отзывы</a></li>
                                <li><a href="{{  route('job') }}" class="uk-margin-small-right"><span class="uk-margin-small-right" uk-icon="file-text"></span>Вакансии</a></li>
                                <li><a href="{{  route('contacts') }}" class="uk-margin-small-right"><span class="uk-margin-small-right" uk-icon="info"></span>Контакты</a></li>
                                <li><a href="{{  route('login') }}"><span class="uk-margin-small-right" uk-icon="sign-in"></span>Логин</a> | <a href="{{  route('register') }}"><span class="uk-margin-small-right" uk-icon="users"></span>Регистрация</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
            <main>
                @yield('content')
            </main>
            <footer>
                <p>&copy; Агенство недвижимости "Ключи". <?=date('Y')?>. By Mozharovskyi Denis</p>
            </footer>
        </div>
    </body>
@endif
</html>
