<div class="uk-margin">
    @if(Session::has('message'))
        <div class="uk-alert-success" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>{{ Session::get('message') }}</p>
        </div>
    @endif
    @if(Session::has('message_danger'))
        <div class="uk-alert-danger" uk-alert>
            <a class="uk-alert-close" uk-close></a>
            <p>{{ Session::get('message_danger') }}</p>
        </div>
    @endif
    @if($errors->any())
        <div class="uk-alert-danger" uk-alert>
            <ul>
                @foreach($errors->all() as $error)
                    <a class="uk-alert-close" uk-close></a>
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>