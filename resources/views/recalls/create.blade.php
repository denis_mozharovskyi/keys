@extends('layouts.app')

@section('content')
    @include('layouts.message')
    <section uk-height-viewport="expand: true">
        <form action="{{ route('recalls.store') }}" method="POST" class="uk-form-horizontal uk-margin-large uk-inline">
            @csrf
            <div class="uk-margin uk-position-center">
                <h4>Форма отзыва</h4>
                <hr>
                <div class="uk-margin">
                    <label for="firstname" class="uk-form-label">Имя</label>
                    <div class="uk-form-controls">
                        <input id="firstname" type="text" name="firstname" class="uk-input uk-form-width-large"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <label for="surname" class="uk-form-label">Фамилия</label>
                    <div class="uk-form-controls">
                        <input id="surname" type="text" name="surname" class="uk-input uk-form-width-large"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <label for="recall_email" class="uk-form-label">Email</label>
                    <div class="uk-form-controls">
                        <input id="recall_email" type="text" name="email" class="uk-input uk-form-width-large"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <label for="recall" class="uk-form-label">Ваш отзыв</label>
                    <div class="uk-form-controls">
                        <textarea id="recall" name="recall" class="uk-textarea uk-form-width-large" rows="5" maxlength="1000" placeholder="Отзыв"></textarea>
                    </div>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Добавить</button>
                </div>
            </div>
        </form>
    </section>
@endsection