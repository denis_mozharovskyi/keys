@extends('layouts.app')

@section('content')
    <section class="uk-section">
        @include('layouts.message')
        <div class="uk-margin uk-flex uk-flex-right">
            <a class="uk-button uk-button-primary uk-button-large uk-margin-small-top" href="{{url('recalls/create')}}">&#43; Добавить</a>
        </div>
        <h2>Отзывы</h2>
        <hr>
        <div class="uk-margin">
            @foreach($recalls as $recall)
                <div class="uk-margin">
                    <h4>{{ $recall->firstname }}&#32;{{ $recall->surname }}</h4>
                    <textarea class="uk-textarea uk-form-width" rows="5" maxlength="1000">{{ $recall->recall }}</textarea>
                    @if(Auth::check())
                        <div class="uk-margin uk-flex uk-flex-right">
                            <a class="blue" href="{{URL::to('recalls/'.$recall->id).'/edit'}}" title="Редактировать" uk-icon="pencil"></a>
                            <form action="{{ route('recalls.destroy', $recall->id) }}" method="POST" class="uk-form-horizontal" >
                                @csrf
                                {{method_field('DELETE')}}
                                <a href="" class="red uk-margin-small-left" title="Удалить" uk-icon="trash"></a>
                            </form>
                        </div>
                    @endif
                    <hr>
                </div>
            @endforeach
        </div>
    </section>
@endsection