@extends('layouts.app')

@section('content')
    <section class="uk-section">
        @include('layouts.message')
        <form action="{{ route('recalls.update', $recall->id) }}" method="POST" class="uk-form-horizontal uk-margin-large">
            @csrf
            @method('PUT')
            <div class="uk-margin">
                <h4>Отзыв</h4>
                <hr>
                <label for="firstname" class="uk-form-label">Имя</label>
                <div class="uk-form-controls">
                    <input id="firstname" type="text" name="firstname" class="uk-input uk-form-width-large" value="{{ $recall->firstname }}"><br>
                </div>
                <label for="surname" class="uk-form-label">Фамилия</label>
                <div class="uk-form-controls">
                    <input id="surname" type="text" name="surname" class="uk-input uk-form-width-large" value="{{ $recall->surname }}"><br>
                </div>
                <label for="email" class="uk-form-label">Email</label>
                <div class="uk-form-controls">
                    <input id="email" type="text" name="email" class="uk-input uk-form-width-large" value="{{ $recall->email }}"><br>
                </div>
                <div class="uk-margin">
                    <label for="recall" class="uk-form-label">Ваш отзыв</label>
                    <div class="uk-form-controls">
                        <textarea id="recall" name="recall" class="uk-textarea uk-form-width-large" rows="5" maxlength="1000">{{ $recall->recall }}</textarea>
                    </div>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Сохранить</button>
                </div>
            </div>
        </form>
    </section>
@endsection