@extends('layouts.app')

@section('content')
    @include('layouts.message')
    <section class="uk-section" uk-height-viewport="expand: true">
        <form action="{{ route('admin.employees.store') }}" method="POST" class="uk-form-horizontal uk-margin-large uk-inline">
            @csrf
            <div class="uk-margin uk-position-center">
                <h4>Сотрудник</h4>
                <hr>
                <div class="uk-margin">
                    <label for="employee_firstname" class="uk-form-label">Имя</label>
                    <div class="uk-form-controls">
                        <input id="employee_firstname" type="text" name="firstname" class="uk-input uk-form-width-large"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <label for="employee_surname" class="uk-form-label">Фамилия</label>
                    <div class="uk-form-controls">
                        <input id="employee_surname" type="text" name="surname" class="uk-input uk-form-width-large"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <label for="employee_phone" class="uk-form-label">Телефон</label>
                    <div class="uk-form-controls">
                        <input id="employee_phone" type="text" name="phone" class="uk-input uk-form-width-large" placeholder="XXX XXX XX XX"><br>
                    </div>
                </div>
                <div hidden>
                    <input type="number" name="client" value="0"><br>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Добавить</button>
                </div>
            </div>
        </form>
    </section>
@endsection