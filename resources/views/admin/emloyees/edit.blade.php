@extends('layouts.app')

@section('content')
    <section class="uk-section">
        @include('layouts.message')
        <form action="{{ route('admin.employees.update', $employee->id) }}" method="POST" class="uk-form-horizontal uk-margin-large">
            @csrf
            @method('PUT')
            <div class="uk-margin">
                <h4>Сотрудник</h4>
                <hr>
                <label for="firstname" class="uk-form-label">Имя</label>
                <div class="uk-form-controls">
                    <input id="firstname" type="text" name="firstname" class="uk-input uk-form-width-large" value="{{ $employee->firstname }}"><br>
                </div>
                <label for="surname" class="uk-form-label">Фамилия</label>
                <div class="uk-form-controls">
                    <input id="surname" type="text" name="surname" class="uk-input uk-form-width-large" value="{{ $employee->surname }}"><br>
                </div>
                <label for="phone" class="uk-form-label">Телефон</label>
                <div class="uk-form-controls">
                    <input id="phone" type="text" name="phone" class="uk-input uk-form-width-large" value="{{ $employee->phone }}"><br>
                </div>
                <div hidden>
                    <input type="number" name="client" value="0"><br>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Сохранить</button>
                </div>
            </div>
        </form>
    </section>
@endsection