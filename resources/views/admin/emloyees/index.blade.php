@extends('layouts.app')

@section('content')
    @include('layouts.message')
    <div class="uk-overflow-auto">
        <table class="uk-table uk-table-small uk-table-hover uk-table-divider">
            <caption><h2>Сотрудники</h2></caption>
            <a class="uk-button uk-button-primary uk-button-large uk-align-right uk-margin-small-top" href="{{url('admin/emloyees/create')}}">&#43; Добавить</a>
            <thead>
                <tr>
                    <th class="uk-width-auto">ID</th>
                    <th class="uk-width-auto">Имя</th>
                    <th class="uk-width-auto">Фамилия</th>
                    <th class="uk-width-auto">Телефон</th>
                    <th class="uk-width-auto">Дата добавл.</th>
                    <th class="uk-width-auto">Дата редактир.</th>
                    <th class="uk-width-auto uk-table-expand">Управление</th>
                    <th class="uk-width-auto"></th>
                </tr>
            </thead>
            <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{ $employee->id }}</td>
                        <td>{{ $employee->firstname }}</td>
                        <td>{{ $employee->surname }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td>{{ $employee->created_at }}</td>
                        <td>{{ $employee->updated_at }}</td>
                        <td><a class="uk-button uk-button-primary uk-button-small" href="{{URL::to('admin/emloyees/'.$employee->id).'/edit'}}">Редактировать</a></td>
                        <td>
                            <form action="{{ route('admin.employees.destroy', $employee->id) }}" method="POST" class="uk-form-horizontal uk-margin-large" >
                                @csrf
                                {{method_field('DELETE')}}
                                <button class="uk-button uk-button-danger uk-button-small">Удалить</button>
                            </form>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection