@extends('layouts.app')

@section('content')
    @include('layouts.message')
    <section class="uk-section">
            <form action="{{ route('admin.items.store') }}" method="POST" enctype="multipart/form-data" class="uk-form-horizontal uk-margin-large">
                @csrf
                <h4>Объект</h4>
                <hr>
                <div class="uk-margin">
                    <label for="action" class="uk-form-label">Действие</label>
                    <div class="uk-form-controls">
                        <select id="action" class="uk-select uk-form-width-large" name="action">
                            <option selected disabled>Любое</option>
                            @foreach($actions as $action)
                                <option value="{{$action->id}}">{{ $action->action_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <city-regions :regions="{{$regions}}"></city-regions>

                    <label for="quarter" class="uk-form-label">Тип недвижимости</label>
                    <div class="uk-form-controls">
                        <select id="quarter" class="uk-select uk-form-width-large" name="quarter">
                            <option selected disabled>Любой</option>
                            @foreach($quarters as $quarter)
                                <option value="{{$quarter->id}}">
                                    {{ $quarter->quarter_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <label for="status" class="uk-form-label">Состояние</label>
                    <div class="uk-form-controls">
                        <select id="status" class="uk-select uk-form-width-large" name="status">
                            <option selected disabled>Любое</option>
                            @foreach($statuses as $status)
                                <option value="{{$status->id}}">{{ $status->status_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="rooms" class="uk-form-label">Количество комнат</label>
                    <div class="uk-form-controls">
                        <input id="rooms" type="number" name="rooms" class="uk-input uk-form-width-large">
                    </div>

                    <label for="number_floors" class="uk-form-label">Этаж</label>
                    <div class="uk-form-controls">
                        <input id="number_floors" type="number" name="number_floors" class="uk-input uk-form-width-large">
                    </div>

                    <label for="floors" class="uk-form-label">Этажность</label>
                    <div class="uk-form-controls">
                        <input id="floors" type="number" name="floors" class="uk-input uk-form-width-large">
                    </div>

                    <label for="price" class="uk-form-label">Цена</label>
                    <div class="uk-form-controls">
                        <input id="price" type="number" name="price" class="uk-input uk-form-width-large">
                    </div>

                    <label for="address" class="uk-form-label">Адресс</label>
                    <div class="uk-form-controls">
                        <input id="address" type="text" name="address" class="uk-input uk-form-width-large" placeholder="ул. Строителей, д. 5, кв. 144">
                    </div>

                    <label for="square" class="uk-form-label">Площадь</label>
                    <div class="uk-form-controls">
                        <input id="square" type="number" name="square" class="uk-input uk-form-width-large">
                    </div>

                    <label for="specification" class="uk-form-label">Описание</label>
                    <div class="uk-form-controls">
                        <textarea id="specification" name="specification" class="uk-textarea uk-form-width-large" rows="5" maxlength="1000" placeholder="Описание"></textarea>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-margin">
                        <label class="uk-form-label">Фото</label>
                    </div>
                    <div class="uk-form-controls" uk-form-custom="target: true">
                        <input type="file" name="image[]" multiple>
                        <input class="uk-input  uk-form-width-large" type="text" placeholder="Выберите файл(ы)" disabled>
                    </div>
                </div>
                <div class="uk-margin">
                    <h4>Клиент</h4>
                    <hr>
                    <label for="client_firstname" class="uk-form-label">Имя</label>
                    <div class="uk-form-controls">
                        <input id="client_firstname" type="text" name="firstname" class="uk-input uk-form-width-large"><br>
                    </div>
                    <label for="client_surname" class="uk-form-label">Фамилия</label>
                    <div class="uk-form-controls">
                        <input id="client_surname" type="text" name="surname" class="uk-input uk-form-width-large"><br>
                    </div>
                    <label for="client_phone" class="uk-form-label">Телефон</label>
                    <div class="uk-form-controls">
                        <input id="client_phone" type="text" name="phone" class="uk-input uk-form-width-large" placeholder="XXX XXX XX XX"><br>
                    </div>
                    <div hidden>
                        <input type="number" name="client" value="1"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <h4>Сотрудник</h4>
                    <hr>
                    <label for="employee" class="uk-form-label">Сотрудник:</label>
                    <div class="uk-form-controls">
                        <select id="employee" class="uk-select uk-form-width-large" name="employee">
                            <option selected disabled>Выберите сотрудника</option>
                            @foreach($employees as $employee)
                                <option value="{{$employee->id}}">{{ $employee->firstname . ' ' . $employee->surname }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Добавить</button>
                </div>
            </form>
    </section>
@endsection