@extends('layouts.app')

@section('content')
    <section class="uk-section">
        @include('layouts.message')
        <div class="uk-child-width " uk-grid>
            <div class="uk-margin-small-top">
                @if($item->photos->isNotEmpty())
                    <div uk-slideshow="animation: push">
                        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">

                            <ul class="uk-slideshow-items">
                                @foreach($item->photos as $photo)
                                    <li>
                                        <img src="{{ asset($photo->src_photo) }}" alt="Photo" uk-cover>
                                    </li>
                                @endforeach
                            </ul>

                            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

                        </div>

                        <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>

                    </div>
                @endif
            </div>
            <form action="{{ route('admin.items.update', $item) }}" method="POST" enctype="multipart/form-data" class="uk-form-horizontal uk-margin-large">
                @csrf
                @method('PUT')
                <h4>Объект</h4>
                <hr>
                @if(Auth::check() && $item->deleted == 1)
                    <h3 style="color: red;">Объект удален!</h3>
                @endif
                <div class="uk-margin">
                    <label for="action" class="uk-form-label">Действие</label>
                    <div class="uk-form-controls">
                        <select id="action" class="uk-select uk-form-width-large" name="action">
                            <option selected disabled>Любое</option>
                            @foreach($actions as $action)
                                <option value="{{$action->id}}" {{ $item->action_id == $action->id ? 'selected' : '' }}>{{ $action->action_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="city" class="uk-form-label">Город</label>
                    <div class="uk-form-controls">
                        <select id="city" class="uk-select uk-form-width-large" name="city">
                            <option disabled selected value="">Любой</option>
                            @foreach($regions as $region)
                                <option value={{$region->id}} {{($region->id == $item_city->id)?'selected':''}} {{($region->disabled == 1)? 'disabled': ''}}>
                                    {{ $region->region_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <label for="region" class="uk-form-label">Район города</label>
                    <div class="uk-form-controls">
                        <select id="region" class="uk-select uk-form-width-large" name="region">
                            <option disabled selected value="">Любой</option>
                            @foreach($regions as $region)
                                @foreach($region->children as $reg)
                                    <option value={{$reg->id}}  {{($reg->id == $item->region_id)?'selected':''}}>{{ $reg->region_name }}</option>
                                @endforeach
                            @endforeach
                        </select>
                    </div>

                    <label for="quarter" class="uk-form-label">Тип недвижимости</label>
                    <div class="uk-form-controls">
                        <select id="quarter" class="uk-select uk-form-width-large" name="quarter">
                            <option selected disabled>Любой</option>
                            @foreach($quarters as $quarter)
                                <option value="{{$quarter->id}}" {{ $item->quarter_id == $quarter->id ? 'selected' : '' }}>
                                    {{ $quarter->quarter_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <label for="status" class="uk-form-label">Состояние</label>
                    <div class="uk-form-controls">
                        <select id="status" class="uk-select uk-form-width-large" name="status">
                            <option selected disabled>Любое</option>
                            @foreach($statuses as $status)
                                <option value="{{$status->id}}" {{ $item->status_id == $status->id ? 'selected' : '' }}>{{ $status->status_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="rooms" class="uk-form-label">Количество комнат</label>
                    <div class="uk-form-controls">
                        <input id="rooms" type="number" name="rooms" class="uk-input uk-form-width-medium" value="{{ $item->rooms }}">
                    </div>
                    @if($item->number_floors)
                        <label for="number_floors" class="uk-form-label">Этаж</label>
                        <div class="uk-form-controls">
                            <input id="number_floors" type="number" name="number_floors" class="uk-input uk-form-width-medium" value="{{ $item->number_floors }}">
                        </div>
                    @endif
                    @if($item->floors)
                        <label for="floors" class="uk-form-label">Этажность</label>
                        <div class="uk-form-controls">
                            <input id="floors" type="number" name="floors" class="uk-input uk-form-width-medium" value="{{ $item->floors }}">
                        </div>
                    @endif
                    <label for="price" class="uk-form-label">Цена</label>
                    <div class="uk-form-controls">
                        <input id="price" type="number" name="price" class="uk-input uk-form-width-medium" value="{{ $item->price }}">
                    </div>

                    <label for="address" class="uk-form-label">Адресс</label>
                    <div class="uk-form-controls">
                        <input id="address" type="text" name="address" class="uk-input uk-form-width-large" value="{{ $item->address }}">
                    </div>

                    <label for="square" class="uk-form-label">Площадь</label>
                    <div class="uk-form-controls">
                        <input id="square" type="number" name="square" class="uk-input uk-form-width-medium" value="{{ $item->square }}">
                    </div>

                    <label for="specification" class="uk-form-label">Описание</label>
                    <div class="uk-form-controls">
                        <textarea id="specification" name="specification" class="uk-textarea" rows="5" maxlength="1000" placeholder="Описание">{{ $item->specification }}</textarea>
                    </div>
                </div>
                <div class="uk-margin">
                    <div>
                        <label class="uk-form-label">Фото</label>
                    </div>
                    <div uk-form-custom="target: true">
                        <input type="file" name="image[]" multiple>
                        <input class="uk-input  uk-form-width-medium" type="text" placeholder="Выберите файл(ы)" disabled>
                    </div>
                </div>
                <div class="uk-margin">
                    <h4>Клиент</h4>
                    <hr>
                    <label for="firstname" class="uk-form-label">Имя</label>
                    <div class="uk-form-controls">
                        <input id="firstname" type="text" name="firstname" class="uk-input uk-form-width-large" value="{{ $item->client->firstname }}"><br>
                    </div>
                    <label for="surname" class="uk-form-label">Фамилия</label>
                    <div class="uk-form-controls">
                        <input id="surname" type="text" name="surname" class="uk-input uk-form-width-large" value="{{ $item->client->surname }}"><br>
                    </div>
                    <label for="phone" class="uk-form-label">Телефон</label>
                    <div class="uk-form-controls">
                        <input id="phone" type="text" name="phone" class="uk-input uk-form-width-large" value="{{ $item->client->phone }}"><br>
                    </div>
                    <div hidden>
                        <input type="number" name="client" value="1"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <h4>Сотрудник</h4>
                    <hr>
                    <label for="employee" class="uk-form-label">Сотрудник:</label>
                    <div class="uk-form-controls">
                        <select id="employee" class="uk-select uk-form-width-large" name="employee">
                            <option selected disabled>Выберите сотрудника</option>
                            @foreach($employees as $employee)
                                <option value="{{$employee->id}}" {{ $item->employee_id == $employee->id ? 'selected' : '' }}>{{ $employee->firstname . ' ' . $employee->surname }}  </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Сохранить</button>
                </div>
            </form>
        </div>
    </section>
@endsection
