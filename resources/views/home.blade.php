@extends('layouts.app')

@section('content')
    @include('layouts.message')
    @include('site.search')
    <section class="uk-section">
        @if(!empty($items))
            @foreach($items as $item)
                @if(Auth::check() && $item->trashed())
                    <h3 style="color: red;">Объект удален!</h3>
                @endif
                <div class="uk-child-width-1-3 uk-inline" uk-grid>
                    <div class="uk-margin">
                        @if($item->photos->isNotEmpty())
                            @if(Auth::check())
                                <div uk-slideshow="animation: push">
                                    <div class="uk-position-relative uk-visible-toggle" tabindex="-1">
                                        <ul class="uk-slideshow-items">
                                            @foreach($item->photos as $photo)
                                                <li>
                                                    <img src="{{ asset($photo->src_photo) }}" alt="Photo" uk-cover>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                                        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
                                    </div>
                                    <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
                                </div>
                            @else
                                <div class="uk-child-width-1-1">
                                    <img src="{{ asset($item->photos[0]->src_photo) }}" alt="Photo">
                                </div>
                            @endif
                        @else
                            <div class="uk-child-width-1-1">
                                <img src="{{ asset('img/no_photo.jpg')}}" alt="Photo">
                            </div>
                        @endif
                    </div>

                    <div class="uk-margin-small">
                        <span>Дата публикации:</span>
                        <span>{{ \Carbon\Carbon::parse($item->created_at)->format('d.m.Y') }}</span>
                        <br>@if(!Auth::check())<br>@endif
                        <span>Действие:</span>
                        <span>{{ $item->action->action_name }}</span>
                        <br>@if(!Auth::check())<br>@endif
                        <span>Город:</span>
                        @foreach($regions as $region)
                            @if($region->id == $item->region->parent_id)
                                <span>{{$region->region_name }}</span>
                            @endif
                        @endforeach
                        <br>
                        <span>Район города:</span>
                        <span>{{ $item->region->region_name }}</span>
                        <br>@if(!Auth::check())<br>@endif
                        <span>Тип недвижимости:</span>
                        <span>{{ $item->quarter->quarter_name }}</span>
                        <br>
                        <span>Состояние:</span>
                        <span>{{ $item->status->status_name }}</span>
                        <br>
                        @if(Auth::check())
                            <span>Количество комнат:</span>
                            <span>{{ $item->rooms }}</span>
                            <br>
                            @if($item->number_floors)
                                <span>Этаж:</span>
                                <span>{{ $item->number_floors }}</span>
                            @endif
                    </div>
                        <div class="uk-margin-small">
                            @if($item->floors)
                                <span>Этажность:</span>
                                <span>{{ $item->floors }}</span>
                            <br>
                            @endif
                            <span>Цена:</span>
                            <span>{{ $item->price }}</span>
                            <br>
                            <span>Адресс:</span>
                            <span>{{ $item->address }}</span>
                            <br>
                            <span>Площадь:</span>
                            <span>{{ $item->square }}</span>
                        <br>
                            <span>Описание:</span>
                            <textarea class="uk-textarea uk-margin-small-top" rows="5" maxlength="1000" >{{ $item->specification }}</textarea>
                        @endif
                        </div>
                    <div class="uk-margin-small">
                        <div class="uk-margin-small-bottom">
                            <strong>Риелтор:</strong>
                        </div>
                        <div class="uk-margin-small-bottom">
                            <span>{{ $item->employee->firstname }}&#32;</span>
                            <span>{{ $item->employee->surname }}</span>
                        <br>@if(!Auth::check())<br>@endif
                            <span>Тел:</span>
                            <span>{{ $item->employee->phone }}</span>
                        </div>
                        @if(!Auth::check())
                            <br><br><a class="uk-button uk-button-primary uk-button-small" href="{{ route('item_show', $item) }}">Подробнее &#187;</a>
                        @endif
                    </div>
                    @if(Auth::check())
                        <div class="uk-margin-small">
                            <div class="uk-margin-small-bottom">
                                <strong>Клиент:</strong>
                            </div>
                            <div class="uk-margin-small-bottom">
                                <span>{{ $item->client->firstname }}&#32;</span>
                                <span>{{ $item->client->surname }}</span>
                            <br>
                                <span>Тел:</span>
                                <span>{{ $item->client->phone }}</span>
                            </div>
                        </div>
                        <div class="uk-margin-small">
                            <div class="uk-margin-small-bottom">
                                <strong>Управление:</strong>
                            </div>
                            <div class="uk-flex uk-flex-right uk-flex-middle">
                                <a class="blue" href="{{URL::to('admin/items/'.$item->id).'/edit'}}" title="Редактировать" uk-icon="pencil"></a>

                                @if($item->trashed())
                                    <form action="{{ route('admin.item_recovery', $item->id) }}" method="POST" class="uk-margin-small-top" >
                                        @csrf
                                        <a href="" class="green uk-button uk-button-small uk-margin-small-left" title="Восстановить" uk-icon="pull"></a>
                                    </form>
                                @else
                                    <form action="{{ route('admin.item_delete', $item) }}" method="POST" class="uk-margin-small-top" >
                                        @csrf
                                        {{method_field('DELETE')}}
                                        <a href="" class="red uk-button uk-button-small uk-margin-small-left" title="Удалить" uk-icon="trash"></a>
                                    </form>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
                <hr>
            @endforeach
            {{ $items->links() }}
        @endif
    </section>
    {{--{{var_damp(\DB::getQueryLog())}}--}}
@endsection