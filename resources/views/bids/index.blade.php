@extends('layouts.app')

@section('content')
    @include('layouts.message')
    @if(Auth::check())
        <section class="uk-section">
            @if($items->isNotEmpty())
                <h2>Заявки</h2>
                <hr>
                @foreach($items as $item)
                    <div class="uk-margin-small">
                        <span>Действие:</span>
                        <span>{{ $item->action->action_name }}</span>
                        <br>
                        <span>Город:</span>
                        @foreach($regions as $region)
                            @if($region->id == $item->region->parent_id)
                                <span>{{$region->region_name }}</span>
                            @endif
                        @endforeach
                        <br>
                        <span>Район города:</span>
                        <span>{{ $item->region->region_name }}</span>
                        <br>
                        <span>Тип недвижимости:</span>
                        <span>{{ $item->quarter->quarter_name }}</span>
                        <br>
                        <span>Состояние:</span>
                        <span>{{ $item->status->status_name }}</span>
                        <br>
                        <span>Количество комнат:</span>
                        <span>{{ $item->rooms }}</span>
                        <br>
                        @if($item->number_floors)
                            <span>Этаж:</span>
                            <span>{{ $item->number_floors }}</span>
                            <br>
                        @endif
                        @if($item->floors)
                            <span>Этажность:</span>
                            <span>{{ $item->floors }}</span>
                            <br>
                        @endif
                        <span>Цена:</span>
                        <span>{{ $item->price }}</span>
                        <br>
                        <span>Адресс:</span>
                        <span>{{ $item->address }}</span>
                        <br>
                        <span>Площадь:</span>
                        <span>{{ $item->square }}</span>
                        <br>
                        <span>Описание:</span>
                        <textarea class="uk-textarea uk-margin-small-top" rows="5" maxlength="1000" >{{ $item->specification }}</textarea>
                    </div>
                    <div class="uk-margin-small">
                        <div class="uk-margin-small-bottom">
                            <strong>Клиент:</strong>
                        </div>
                        <div class="uk-margin-small-bottom">
                            <span>{{ $item->client->firstname }}&#32;</span>
                            <span>{{ $item->client->surname }}</span>
                            <br>
                            <span>Тел:</span>
                            <span>{{ $item->client->phone }}</span>
                        </div>
                    </div>
                    <div class="uk-margin uk-flex uk-flex-right">
                        <a class="green" href="{{URL::to('bids/'.$item->id).'/edit'}}" title="Добавить объект" uk-icon="plus-circle"></a>
                        <form action="{{ route('bids.destroy', $item->id) }}" method="POST" class="uk-form-horizontal" >
                            @csrf
                            {{method_field('DELETE')}}
                            <a href="" class="red uk-button uk-button-small uk-margin-small-left" title="Удалить заявку" uk-icon="trash"></a>
                        </form>
                    </div>
                    <hr>
                @endforeach
            @else
                <div uk-height-viewport="expand: true">
                    <div class="uk-inline">
                        <h2 class="uk-position-center">Заявок нет.</h2>
                    </div>
                </div>
            @endif
        </section>
    @else
        <section class="uk-section">
            <form action="{{ route('bids.store') }}" method="POST" enctype="multipart/form-data" class="uk-form-horizontal uk-margin-large">
                @csrf
                <h4>Объект</h4>
                <hr>
                <div class="uk-margin">
                    <label for="action" class="uk-form-label">Действие</label>
                    <div class="uk-form-controls">
                        <select id="action" class="uk-select uk-form-width-large" name="action">
                            <option selected disabled>Любое</option>
                            @foreach($actions as $action)
                                <option value="{{$action->id}}">{{ $action->action_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <city-regions :regions="{{$regions}}"></city-regions>

                    <label for="quarter" class="uk-form-label">Тип недвижимости</label>
                    <div class="uk-form-controls">
                        <select id="quarter" class="uk-select uk-form-width-large" name="quarter">
                            <option selected disabled>Любой</option>
                            @foreach($quarters as $quarter)
                                <option value="{{$quarter->id}}">
                                    {{ $quarter->quarter_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <label for="status" class="uk-form-label">Состояние</label>
                    <div class="uk-form-controls">
                        <select id="status" class="uk-select uk-form-width-large" name="status">
                            <option selected disabled>Любое</option>
                            @foreach($statuses as $status)
                                <option value="{{$status->id}}">{{ $status->status_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="rooms" class="uk-form-label">Количество комнат</label>
                    <div class="uk-form-controls">
                        <input id="rooms" type="number" name="rooms" class="uk-input uk-form-width-large">
                    </div>

                    <label for="number_floors" class="uk-form-label">Этаж</label>
                    <div class="uk-form-controls">
                        <input id="number_floors" type="number" name="number_floors" class="uk-input uk-form-width-large">
                    </div>

                    <label for="floors" class="uk-form-label">Этажность</label>
                    <div class="uk-form-controls">
                        <input id="floors" type="number" name="floors" class="uk-input uk-form-width-large">
                    </div>

                    <label for="price" class="uk-form-label">Цена</label>
                    <div class="uk-form-controls">
                        <input id="price" type="number" name="price" class="uk-input uk-form-width-large">
                    </div>

                    <label for="address" class="uk-form-label">Адресс</label>
                    <div class="uk-form-controls">
                        <input id="address" type="text" name="address" class="uk-input uk-form-width-large" placeholder="ул. Строителей, д. 5, кв. 144">
                    </div>

                    <label for="square" class="uk-form-label">Площадь</label>
                    <div class="uk-form-controls">
                        <input id="square" type="number" name="square" class="uk-input uk-form-width-large">
                    </div>

                    <label for="specification" class="uk-form-label">Описание</label>
                    <div class="uk-form-controls">
                        <textarea id="specification" name="specification" class="uk-textarea uk-form-width-large" rows="5" maxlength="1000" placeholder="Описание"></textarea>
                    </div>
                </div>
                <div class="uk-margin">
                    <h4>Клиент</h4>
                    <hr>
                    <label for="client_firstname" class="uk-form-label">Имя</label>
                    <div class="uk-form-controls">
                        <input id="client_firstname" type="text" name="firstname" class="uk-input uk-form-width-large"><br>
                    </div>
                    <label for="client_surname" class="uk-form-label">Фамилия</label>
                    <div class="uk-form-controls">
                        <input id="client_surname" type="text" name="surname" class="uk-input uk-form-width-large"><br>
                    </div>
                    <label for="client_phone" class="uk-form-label">Телефон</label>
                    <div class="uk-form-controls">
                        <input id="client_phone" type="text" name="phone" class="uk-input uk-form-width-large" placeholder="XXX XXX XX XX"><br>
                    </div>
                    <div hidden>
                        <input type="number" name="client" value="1"><br>
                    </div>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small-bottom">Добавить</button>
                </div>
            </form>
        </section>
    @endif
@endsection