@extends('layouts.app')

@section('content')
    <section class="uk-section">
        <div class="uk-child-width" uk-grid>
            <div class="uk-margin">
                @if($item->photos->isNotEmpty())
                    <div uk-slideshow="animation: push">
                        <div class="uk-position-relative uk-visible-toggle" tabindex="-1">

                            <ul class="uk-slideshow-items">
                                @foreach($item->photos as $photo)
                                    <li>
                                        <img src="{{ asset($photo->src_photo) }}" alt="Photo" uk-cover>
                                    </li>
                                @endforeach
                            </ul>

                            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

                        </div>

                        <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>

                    </div>
                @else
                    <div class="uk-flex uk-flex-center">
                        <img data-src="{{ $photo_url->url('images/no_photo.jpg')}}" alt="Photo" uk-img>
                    </div>
                @endif
            </div>
            <div class="uk-margin-small">
                <span>Дата публикации:</span>
                <span>{{ \Carbon\Carbon::parse($item->created_at)->format('d.m.Y') }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Действие:</span>
                <span>{{ $item->action->action_name }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Город:</span>
                @foreach($regions as $region)
                    @if($region->id == $item->region->parent_id)
                        <span>{{$region->region_name }}</span>
                    @endif
                @endforeach
            </div>
            <div class="uk-margin-small">
                <span>Район города:</span>
                <span>{{ $item->region->region_name }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Тип недвижимости:</span>
                <span>{{ $item->quarter->quarter_name }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Состояние:</span>
                <span>{{ $item->status->status_name }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Количество комнат:</span>
                <span>{{ $item->rooms }}</span>
            </div>
            @if($item->number_floors)
                <div class="uk-margin-small">
                    <span>Этаж:</span>
                    <span>{{ $item->number_floors }}</span>
                </div>
            @endif
            @if($item->floors)
                <div class="uk-margin-small">
                    <span>Этажность:</span>
                    <span>{{ $item->floors }}</span>
                </div>
            @endif
            <div class="uk-margin-small">
                <span>Цена:</span>
                <span>{{ $item->price }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Адресс:</span>
                <span>{{ explode(',', $item->address)[0] }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Площадь:</span>
                <span>{{ $item->square }}</span>
            </div>
            <div class="uk-margin-small">
                <span>Описание:</span>
                <textarea class="uk-textarea uk-margin-small-top" rows="5" maxlength="1000" >{{ $item->specification }}</textarea>
            </div>
            <div class="uk-margin-small">
                <div class="uk-margin-small-bottom">
                    <strong>Риелтор:</strong>
                </div>
                <div class="uk-margin-small-bottom">
                    <span>Имя:</span>
                    <span>{{ $item->employee->firstname }}</span>
                    <br>
                    <span>Фамилия:</span>
                    <span>{{ $item->employee->surname }}</span>
                    <br>
                    <span>Телефон:</span>
                    <span>{{ $item->employee->phone }}</span>
                </div>
            </div>
        </div>
    </section>
    {{--{{var_damp(\DB::getQueryLog())}}--}}
@endsection