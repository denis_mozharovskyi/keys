<section class=" uk-margin">
    <div class="uk-flex uk-flex-right">
        <button class="uk-button uk-button-small" uk-toggle="target: .nav-overlay; animation: uk-animation-fade">
            <span uk-icon="icon: triangle-down"></span>
            <span>Поиск</span>
        </button>
    </div>
    <div class="nav-overlay uk-navbar-left uk-flex-1" hidden>
        <div class="uk-navbar-item uk-width-expand">
            <form action="" method="GET" class="uk-form-horizontal uk-margin-small">
                    @if(Auth::check())
                        <h4>Объект</h4>
                        <hr>
                    @endif
                    <div class="uk-margin">
                        <label for="action" class="uk-form-label">Действие</label>
                        <div class="uk-form-controls">
                            <select id="action" class="uk-select uk-form-width-large" name="action">
                                <option disabled selected>Любое</option>
                                @foreach($actions as $action)
                                    <option value="{{$action->id}}" {{ request('action') == $action->id ? 'selected' : '' }}>{{ $action->action_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <city-regions :regions="{{$regions}}"></city-regions>

                        <label for="quarter" class="uk-form-label">Тип недвижимости</label>
                        <div class="uk-form-controls">
                            <select id="quarter" class="uk-select uk-form-width-large" name="quarter">
                                <option disabled selected>Любой</option>
                                @foreach($quarters as $quarter)
                                    <option value="{{$quarter->id}}" {{ request('$quarter') == $quarter->id ? 'selected' : '' }}>
                                        {{ $quarter->quarter_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <label for="status" class="uk-form-label">Состояние</label>
                        <div class="uk-form-controls">
                            <select id="status" class="uk-select uk-form-width-large" name="status">
                                <option disabled selected>Любое</option>
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}" {{ request('status') == $status->id ? 'selected' : '' }}>{{ $status->status_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <label for="rooms" class="uk-form-label">Количество комнат</label>
                        <div class="uk-form-controls">
                            <input id="rooms" type="number" name="rooms_from" placeholder="От" class="uk-input uk-form-width-medium" value="{{ request('rooms_from') ? request('rooms_from') : '' }}">
                            &nbsp;
                            <input type="number" name="rooms_to" placeholder="До" class="uk-input uk-form-width-medium" value="{{ request('rooms_to') ? request('rooms_to') : '' }}">
                        </div>

                        <label for="floors" class="uk-form-label">Этаж</label>
                        <div class="uk-form-controls">
                            <input id="floors" type="number" name="number_floors_from" placeholder="От" class="uk-input uk-form-width-medium" value="{{ request('number_floors_from') ? request('number_floors_from') : '' }}">
                            &nbsp;
                            <input type="number" name="number_floors_to" placeholder="До" class="uk-input uk-form-width-medium" value="{{ request('number_floors_to') ? request('number_floors_to') : '' }}">
                        </div>

                        <label for="price" class="uk-form-label">Цена</label>
                        <div class="uk-form-controls">
                            <input id="price" type="number" name="price_from" placeholder="От" class="uk-input uk-form-width-medium" value="{{ request('price_from') ? request('price_from') : '' }}">
                            &nbsp;
                            <input type="number" name="price_to" placeholder="До" class="uk-input uk-form-width-medium" value="{{ request('price_to') ? request('price_to') : '' }}">
                        </div>

                        <div class="uk-form-label">Только с фото</div>
                        <div class="uk-form-controls uk-form-controls-text">
                            <label><input type="checkbox" name="only_photo" {{ request('only_photo') ? 'checked' : '' }}></label>
                        </div>
                    </div>
                    @if(Auth::check())
                        <div class="uk-margin-small">
                            <div class="uk-form-label">Удаленные объекты</div>
                            <div class="uk-form-controls uk-form-controls-text">
                                <label><input type="checkbox" name="deleted" value="deleted"{{ request('deleted') ? 'checked' : '' }}></label>
                            </div>
                        </div>
                    @endif
                    @if(Auth::check())
                        <br>
                        <div class="uk-margin">
                            <h4>Сотрудник</h4>
                            <hr>
                            <label for="employee" class="uk-form-label">Сотрудник:</label>
                            <div class="uk-form-controls">
                                <select id="employee" class="uk-select uk-form-width-large" name="employee">
                                    <option disabled selected>Выберите сотрудника</option>
                                    @foreach($employees as $employee)
                                        <option value="{{$employee->id}}" {{ old('$employee') == $employee->id ? 'selected' : '' }}>{{ $employee->firstname . ' ' . $employee->surname }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="uk-margin">
                        <button type="submit" class="uk-button uk-button-primary uk-width-1-1 uk-margin-small">Поиск</button>
                    </div>
                </form>
        </div>
    </div>
</section>
