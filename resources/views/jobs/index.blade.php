@extends('layouts.app')

@section('content')
    <section class="uk-section">
        <div uk-height-viewport="expand: true">
            <div class="uk-inline">
               <h2 class="uk-position-center">Нет вакансий.</h2>
            </div>
        </div>
    </section>
@endsection