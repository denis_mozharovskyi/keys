import UIkit from 'uikit/dist/js/uikit';
import Icons from 'uikit/dist/js/uikit-icons';

UIkit.use(Icons);

require('./bootstrap');

window.Vue = require('vue');

import CityRegions from './components/CityRegions.vue';

Vue.component('city-regions', CityRegions);

const app = new Vue({
    el: '#app'
});
