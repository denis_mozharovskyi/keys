<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'region_name' => 'Винница',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Днепр',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Донецк',
                'disabled' => 1,
                'children' => [

                ],
            ],
            [
                'region_name' => 'Запорожье',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Ивано-Франковск',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Киев',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Кропивницкий',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Луганск',
                'disabled' => 1,
                'children' => [

                ],
            ],
            [
                'region_name' => 'Луцк',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Львов',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Николаев',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Одесса',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Полтава',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Ровно',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Сумы',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Тернополь',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Ужгород',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Харьков',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Херсон',
                'selected' => 1,
                'children' => [
                    ['region_name' => 'Центр, Мельницы'],
                    ['region_name' => 'Таврический, Северный'],
                    ['region_name' => 'Жилпоселок'],
                    ['region_name' => 'Шуменский'],
                    ['region_name' => 'Восточный, Киндийка'],
                    ['region_name' => 'ХБК, Стеклотара'],
                    ['region_name' => 'Текстильный'],
                    ['region_name' => 'Остров, Нефтегавань'],
                    ['region_name' => 'Речпорт'],
                    ['region_name' => 'Привокзальный'],
                    ['region_name' => 'Военка'],
                ],
            ],
            [
                'region_name' => 'Хмельницкий',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Черкассы',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Чернигов',
                'children' => [

                ],
            ],
            [
                'region_name' => 'Черновцы',
                'children' => [

                ],
            ],
        ];
        foreach($regions as $region)
        {
            \App\Region::create($region);
        }
    }
}
