<?php

use Illuminate\Database\Seeder;

class QuartersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $quarters = [
            ['quarter_name' => 'Квартиры'],
            ['quarter_name' => 'Дома'],
            ['quarter_name' => 'Участки'],
            ['quarter_name' => 'Коммерческая недвижимость'],
        ];
        foreach($quarters as $quarter)
        {
            \App\Quarter::create($quarter);
        }
    }
}
