<?php

use Illuminate\Database\Seeder;

class ActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $actions = [
            ['action_name' => 'Продажа'],
            ['action_name' => 'Аренда']
        ];
        foreach($actions as $action)
        {
            \App\Action::create($action);
        }
    }
}
