<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses =[
            ['status_name' => 'Евроремонт'],
            ['status_name' => 'Жилое'],
            ['status_name' => 'Нуждается в косметическом ремонте'],
            ['status_name' => 'От строителей'],
            ['status_name' => 'Под застройку'],
            ['status_name' => 'Под ремонт'],
            ['status_name' => 'После капитального ремонта'],
            ['status_name' => 'После косметического ремонта'],
        ];
        foreach($statuses as $status)
        {
            \App\Status::create($status);
        }
    }
}
