<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resources([
    'recalls' => 'RecallController',
    'bids' => 'BidController'
]);

// menu
Route::get('/', 'SiteController@index')->name('index');
Route::get('/bids', 'BidController@index')->name('bids');
Route::get('/recalls', 'RecallController@index')->name('recalls');
Route::get('/jobs', 'JobController@index')->name('job');
Route::get('/contacts', 'ContactController@index')->name('contacts');
Route::get('/items/{item}', 'ItemController@show')->name('item_show');

Route::get('recalls/create', 'RecallController@create');


/**************************************************************************************************/

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

//Если нужно разрешить регистрацию других пользователей, тогда нужно ввести флаг для роли админа
Route::any('register', function (){
    return redirect('/');
});


Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function ()
{
    Route::post('items/{item}', 'ItemController@recovery')->name('item_recovery');

    Route::resources([
        'items' => 'ItemController',
        'employees' => 'HumanController',
        'recalls' => 'RecallController'
    ]);

// menu admin
    Route::get('/','SiteController@index');
    Route::get('/bids', 'BidController@index');
    Route::get('/recalls', 'RecallController@index');
    Route::get('/jobs', 'JobController@index');
    Route::get('/contacts', 'ContactController@index');
    Route::get('/emloyees','HumanController@index');
/**************************************************************************************************/

// Item
    Route::get('items/create','ItemController@create');
    Route::get('items/{item}/edit','ItemController@edit');
    Route::put('items/{item}','ItemController@update');
    Route::delete('items/{item}','ItemController@destroy')->name('item_delete');
/**************************************************************************************************/

// Employee
    Route::get('emloyees/create','HumanController@create');
    Route::get('emloyees/{emloyee}/edit','HumanController@edit');
    Route::put('emloyees/{emloyee}','HumanController@update');
    Route::delete('emloyees/{emloyee}','HumanController@destroy');
/**************************************************************************************************/
    // Recall
    Route::get('recalls/create', 'RecallController@create');
    Route::get('recalls/{recall}/edit','RecallController@edit');
    Route::put('recalls/{recall}','RecallController@update');
    Route::delete('recalls/{recall}','RecallController@destroy');

    // Bids
    Route::get('bids/{bid}/edit','BidController@edit');
    Route::put('bids/{bid}','BidController@update');

});



