<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Action;
use App\Quarter;
use App\Region;
use App\Status;
use App\Item;
use App\Human;

class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actions = Action::all();
        $quarters = Quarter::all();
        $regions = Region::get()->toTree();
        $statuses = Status::all();

        $items = Item::whereNull('employee_id')->get();

        return view('bids.index', compact('actions', 'quarters', 'regions', 'statuses', 'items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'price' => 'required|integer',
            'address' => 'required|string',
            'square' => 'required|integer',
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|regex:/^0[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/'
        ]);
        if($request->isMethod('post'))
        {
            $client = new Human([
                'firstname' => $request->get('firstname'),
                'surname' => $request->get('surname'),
                'phone' => $request->get('phone'),
                'client' => $request->get('client')
            ]);
            $client->save();

            $item = new Item([
                'region_id' => $request->get('region'),
                'quarter_id' => $request->get('quarter'),
                'action_id' => $request->get('action'),
                'status_id' => $request->get('status'),
                'client_id' => $client->id,
                'rooms' => $request->get('rooms'),
                'specification' => $request->get('specification'),
                'price' => $request->get('price'),
                'square' => $request->get('square'),
                'address' => $request->get('address'),
                'floors' => $request->get('floors'),
                'number_floors' => $request->get('number_floors')
            ]);
            $item->save();
        }
        return redirect('/bids')->with('message', 'Заявка добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $employees = Human::where('client', 0)->get();
        $actions = Action::all();
        $quarters = Quarter::all();
        $regions = Region::get()->toTree();
        $statuses = Status::all();

        $parent_city = Region::find($item->region_id);
        $item_city = Region::find($parent_city->parent_id);

        return view('bids.edit', compact('item', 'actions', 'quarters', 'employees','regions', 'statuses', 'item_city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::find($id);

        $request->validate([
            'price' => 'required|integer',
            'address' => 'required|string',
            'square' => 'required|integer',
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|regex:/^0[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/',
            'client' => 'required|integer',
            'employee' => 'required|integer'
        ]);

        $client = Human::find($item->client_id);
        $client->update([
            'firstname' => $request->get('firstname'),
            'surname' => $request->get('surname'),
            'phone' => $request->get('phone'),
            'client' => $request->get('client')
        ]);

        $item->update([
            'region_id' => $request->get('region'),
            'quarter_id' => $request->get('quarter'),
            'action_id' => $request->get('action'),
            'status_id' => $request->get('status'),
            'employee_id' => $request->get('employee'),
            'rooms' => $request->get('rooms'),
            'specification' => $request->get('specification'),
            'price' => $request->get('price'),
            'square' => $request->get('square'),
            'address' => $request->get('address'),
            'floors' => $request->get('floors'),
            'number_floors' => $request->get('number_floors')
        ]);

        return redirect('/bids')->with('message', 'Объект добавлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item -> delete();
        return redirect('/bids')->with('message', 'Заявка удалена!');
    }
}
