<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recall;

class RecallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recalls = Recall::orderBy('recalls.created_at', 'desc')->get();
        return view('recalls.index', compact('recalls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recalls.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'email' => 'nullable|email',
            'recall' => 'required'
        ]);

        Recall::create([
            'firstname' => $request->get('firstname'),
            'surname' => $request->get('surname'),
            'email' => $request->get('email'),
            'recall' => $request->get('recall')
        ]);

        return redirect('recalls')->with('message', 'Отзыв добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recall = Recall::find($id);
        return view('recalls.edit', compact('recall'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'email' => 'nullable|email',
            'recall' => 'required'
        ]);

        $recall = Recall::find($id);

        $recall->firstname = $request->firstname;
        $recall->surname = $request->surname;
        $recall->email = $request->email;
        $recall->recall = $request->recall;

        $recall->save();

        return redirect('recalls')->with('message', 'Редактирование успешно сохранено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $recall = Recall::find($id);
        $recall -> delete();
        return redirect('recalls')->with('message', 'Отзыв удален!');
    }
}
