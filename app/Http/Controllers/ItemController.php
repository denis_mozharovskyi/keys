<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Action;
use App\Quarter;
use App\Region;
use App\Status;
use App\Human;
use App\Photo;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Human::where('client', 0)->get();
        $actions = Action::all();
        $quarters = Quarter::all();
        $regions = Region::get()->toTree();
        $statuses = Status::all();
        return view('admin.items.create', compact('actions', 'quarters', 'employees','regions', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'price' => 'required|integer',
            'address' => 'required|string',
            'square' => 'required|integer',
            'image[]' => 'mimes:jpeg,jpg|dimensions:min_width=800,min_height=800',
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|regex:/^0[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/',
            'client' => 'required|integer'
        ]);
        if($request->isMethod('post'))
        {
            $client = new Human([
                'firstname' => $request->get('firstname'),
                'surname' => $request->get('surname'),
                'phone' => $request->get('phone'),
                'client' => $request->get('client')
            ]);
            $client->save();

            $item = new Item([
                'region_id' => $request->get('region'),
                'quarter_id' => $request->get('quarter'),
                'action_id' => $request->get('action'),
                'status_id' => $request->get('status'),
                'client_id' => $client->id,
                'employee_id' => $request->get('employee'),
                'rooms' => $request->get('rooms'),
                'specification' => $request->get('specification'),
                'price' => $request->get('price'),
                'square' => $request->get('square'),
                'address' => $request->get('address'),
                'floors' => $request->get('floors'),
                'number_floors' => $request->get('number_floors')
            ]);
            $item->save();

            if ($request->hasFile('image')) {
                $item->with_photo = 1;
                $item->save();

                $files = $request->file('image');
                foreach ($files as $file) {
                    $photo = new Photo();

                    $extension = $file->getClientOriginalExtension();
                    $fileName = md5_file($file) . '.' . $extension;

                    $imagePath = '/storage/images/' . $fileName;
                    \Image::make($file)->resize(800, 600)->save( storage_path('app/public/images/' . $fileName) );

                    $photo->src_photo = $imagePath;
                    $photo->item_id = $item->id;
                    $photo->save();
                }
            }
        }
        return redirect('/admin/items/create')->with('message', 'Объект добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        \DB::connection()->enableQueryLog();
        $regions = Region::get()->toTree();
        return view('site.show', compact('item', 'regions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $employees = Human::where('client', 0)->get();
        $actions = Action::all();
        $quarters = Quarter::all();
        $regions = Region::get()->toTree();
        $statuses = Status::all();

        $parent_city = Region::find($item->region_id);
        $item_city = Region::find($parent_city->parent_id);

        return view('admin.items.edit', compact('item', 'actions', 'quarters', 'employees','regions', 'statuses', 'item_city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $request->validate([
            'price' => 'required|integer',
            'address' => 'required|string',
            'square' => 'required|integer',
            'image[]' => 'mimes:jpeg,jpg|dimensions:min_width=800,min_height=800',
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|regex:/^0[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/',
            'client' => 'required|integer'
        ]);

        $client = Human::find($item->client_id);
        $client->update([
            'firstname' => $request->get('firstname'),
            'surname' => $request->get('surname'),
            'phone' => $request->get('phone'),
            'client' => $request->get('client')
        ]);

        $item->update([
            'region_id' => $request->get('region'),
            'quarter_id' => $request->get('quarter'),
            'action_id' => $request->get('action'),
            'status_id' => $request->get('status'),
            'employee_id' => $request->get('employee'),
            'rooms' => $request->get('rooms'),
            'specification' => $request->get('specification'),
            'price' => $request->get('price'),
            'square' => $request->get('square'),
            'address' => $request->get('address'),
            'floors' => $request->get('floors'),
            'number_floors' => $request->get('number_floors')
        ]);

        if ($request->hasFile('image')) {
            $files = $request->file('image');
            foreach ($files as $file) {
                $photo = new Photo();

                $extension = $file->getClientOriginalExtension();
                $fileName = md5_file($file) . '.' . $extension;

                $imagePath = '/storage/images/' . $fileName;
                \Image::make($file)->resize(800, 600)->save( storage_path('app/public/images/' . $fileName) );

                $photo->src_photo = $imagePath;
                $photo->item_id = $item->id;
                $photo->save();
            }
        }
        return redirect('admin')->with('message', 'Редактирование успешно сохранено!');
    }

    public function recovery($id)
    {
        Item::onlyTrashed()->find($id)->restore();
        return redirect('admin')->with('message', 'Объект восстановлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->delete();
        return redirect('admin')->with('message', 'Объект удален!');
    }
}
