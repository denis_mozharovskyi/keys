<?php

namespace App\Http\Controllers;

use App\Human;
use App\Item;
use Illuminate\Http\Request;

class HumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees  = Human::where('client', 0)->get();
        return view('admin.emloyees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.emloyees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|regex:/^0[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/',
            'client' => 'required|integer',
        ]);

        $employee = new Human([
            'firstname' => $request->get('firstname'),
            'surname' => $request->get('surname'),
            'phone' => $request->get('phone'),
            'client' => $request->get('client')
        ]);
        $employee->save();

        return redirect('admin/employees')->with('message', 'Сотрудник добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Human::find($id);
        return view('admin.emloyees.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|regex:/^0[0-9]{2}\s[0-9]{3}\s[0-9]{2}\s[0-9]{2}$/',
            'client' => 'required|integer'
        ]);

        $employee =Human::find($id);

        $employee->firstname = $request->firstname;
        $employee->surname = $request->surname;
        $employee->phone = $request->phone;
        $employee->client = $request->client;

        $employee->save();

        return redirect('admin/employees')->with('message', 'Редактирование успешно сохранено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Item::where('employee_id', $id)->get()->isNotEmpty()){
            return redirect('admin/employees')->with('message_danger', 'Сотрудник привязан к объектам!');
        }
        $employee = Human::find($id);
        $employee -> delete();
        return redirect('admin/employees')->with('message', 'Сотрудник удален!');
    }
}
