<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Action;
use App\Quarter;
use App\Region;
use App\Status;
use App\Item;
use App\Human;

class SiteController extends Controller
{
    public function index(Request $request)
    {

        \DB::connection()->enableQueryLog();
        $employees = Human::where('client', 0)->get();
        $actions = Action::all();
        $quarters = Quarter::all();
        $regions = Region::get()->toTree();
        $statuses = Status::all();

        $items = Item::orderBy('items.created_at', 'desc')->whereNotNull('employee_id');

        $request->validate([
            'rooms_from' => 'nullable|integer|min:1',
            'rooms_to' => 'nullable||integer|gte:rooms_from',
            'number_floors_from' => 'nullable|integer|min:1',
            'number_floors_to' => 'nullable|integer|gte:number_floors_from',
            'price_from' => 'nullable|integer|min:1',
            'price_to' => 'nullable|integer|gte:price_from',
        ]);

        if (!empty($request->action)) {
            $items->where('action_id', $request->action);
        }
        if (!empty($request->quarter)) {
            $items->where('quarter_id', $request->quarter);
        }
        if (!empty($request->status)) {
            $items->where('status_id', $request->status);
        }
        if (!empty($request->region)) {
            $items->where('region_id', $request->region);
        }
        if (!empty($request->rooms_from) && !empty($request->rooms_to)) {
            $items->whereBetween('rooms', [$request->rooms_from, $request->rooms_to]);
        }
        if (!empty($request->number_floors_from) && !empty($request->number_floors_to)) {
            $items->whereBetween('number_floors', [$request->number_floors_from, $request->number_floors_to]);
        }
        if (!empty($request->price_from) && !empty($request->price_to)) {
            $items->whereBetween('price', [$request->price_from, $request->price_to]);
        }
        if (\Auth::check() && !empty($request->employee)) {
            $items->where('employee_id', $request->employee);
        }
        if ($request->has('only_photo')) {
            $items->where('with_photo', 1);
        }

        if (\Auth::check() && $request->has('deleted')) {
            $items->onlyTrashed();
        }

        $items = $items->paginate(3)->appends($request->all());

        $photo_url = \Storage::disk('public');
        return view('home', compact('actions', 'quarters', 'employees', 'regions', 'statuses', 'items', 'photo_url'));
    }

}
