<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Region extends Model
{
    protected $guarded = [];

    use NodeTrait;

    public function items()
    {
        return $this->hasMany('App\Item');
    }
}
