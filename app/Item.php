<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    protected $guarded = [];

    use SoftDeletes;

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function action()
    {
        return $this->belongsTo('App\Action');
    }

    public function client()
    {
        return $this->belongsTo('App\Human', 'client_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Human', 'employee_id', 'id');
    }

    public function quarter()
    {
        return $this->belongsTo('App\Quarter');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }
}
